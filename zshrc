# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
bindkey -v
zstyle :compinstall filename '/home/Zakhar/.zshrc'
zstyle ':completion:*' menu select

# При совпадении первых букв слова вывести меню выбора
zstyle ':completion:*' menu select=long-list select=0
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

# Чтобы игнорировать повторяющиеся строки в истории, используйте следующее:
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_REDUCE_BLANKS
# Корректировка ввода
setopt CORRECT_ALL
# просто наберите нужный каталог и окажитесь в нём
setopt autocd

# Позволяем разворачивать сокращенный ввод, к примеру cd /u/sh в /usr/share 
autoload -Uz compinit promptinit
compinit
promptinit

autoload -U pick-web-browser
alias -s {html,htm}=firefox-bin

autoload -U colors && colors
bindkey "^[[3~" delete-char
bindkey ';5D' backward-word # ctrl+left
bindkey ';5C' forward-word #ctrl+right

# Распаковка архивов
# example: extract file
extract () {
 if [ -f $1 ] ; then
 case $1 in
 *.tar.bz2)   tar xjf $1        ;;
 *.tar.gz)    tar xzf $1     ;;
 *.bz2)       bunzip2 $1       ;;
 *.rar)       unrar x $1     ;;
 *.gz)        gunzip $1     ;;
 *.tar)       tar xf $1        ;;
 *.tbz2)      tar xjf $1      ;;
 *.tbz)       tar -xjvf $1    ;;
 *.tgz)       tar xzf $1       ;;
 *.zip)       unzip $1     ;;
 *.Z)         uncompress $1  ;;
 *.7z)        7z x $1    ;;
 *)          echo "I don't know how to extract '$1'..." ;;
 esac
 else
 echo "'$1' is not a valid file"
 fi
} 

# Запаковать архив
# example: pk tar file
pk () {
 if [ $1 ] ; then
 case $1 in
 tbz)       tar cjvf $2.tar.bz2 $2      ;;
 tgz)       tar czvf $2.tar.gz  $2       ;;
 tar)      tar cpvf $2.tar  $2       ;;
 bz2)    bzip $2 ;;
 gz)        gzip -c -9 -n $2 > $2.gz ;;
 zip)       zip -r $2.zip $2   ;;
 7z)        7z a $2.7z $2    ;;
 *)         echo "'$1' cannot be packed via pk()" ;;
 esac
 else
 echo "'$1' is not a valid file"
 fi

}

# export TERM=xterm-256color
alias shutdown='sudo shutdown -P now'
alias reboot='sudo reboot'
alias study='cd ~/Documents/Study/II/'
alias ls='ls -lh --color=auto'
alias ..='cd ..'
alias his='cat ~/.histfile'
alias atr='atril 2>/dev/null 1>/dev/null'
alias grep='grep --colour=auto'
alias -s {avi,mpeg,mpg,mov,m2v}=vlc
alias -s {ppt,pptx,odt,doc,docx,sxw,rtf}=libreoffice
alias brt='sudo ~/.scripts/xes'


export TERM=rxvt-unicode
export PROMPT='%F{6}%n%f%F{6}@%m $%f ' export RPROMPT='%F{6}%~%f'
export max_brt=`cat /sys/class/backlight/intel_backlight/max_brightness`
export STARDICT_DATA_DIR=$XDG_DATA_HOME









